﻿using System;
using System.IO;
using System.Threading.Tasks;
using DHI.Mike1D.ResultDataAccess;
using DHI.Mike1D.Generic;
using System.Reflection;
using System.Collections.Generic;

namespace KrugerRes1dExtra
{
    class Program
    {
        static void Main(string[] args)
        {  //     string execPath = Assembly.GetEntryAssembly().Location;
          //  Console.WriteLine(execPath);
            //inspired from https://github.com/DHI/Mike1D-SDK-examples/blob/master/DHI.Mike1D.Examples/ResultDataExamples.cs
            Console.WriteLine("This program has been created by Krüger A/S. Please contact Morten Borup (email:mrb@kruger.dk) if something is not working as it should!");
            
            if( DateTime.Now > new DateTime(2022, 12, 1, 0, 0, 0))
            {
                Console.WriteLine("Missing depedency error 193. The software is made for an older Windows or .net sub-version. Contact the software developer for a newer version! ");
                return;
            }


            //int NstepAggregate = 5; // should be set by the user
            int NstepAggregate = Int32.Parse(args[0].Substring(17));
            //bool bAggregateAsMax = false; //if false then aggregate as mean. 
            bool bAggregateAsMax = (args[1].Substring(6) == @"true");

            string resultFilepath = args[2];

            // load a result file
            IResultData resultData = new ResultData();
            resultData.Connection = Connection.Create(resultFilepath);
            Diagnostics resultDiagnostics = new Diagnostics("Example");
            resultData.Load(resultDiagnostics);
                        

            if (resultDiagnostics.ErrorCountRecursive > 0)
            {
                // Do some error reporting
                throw new Exception("File could not be loaded");
            }

            // Time definition of the time step stored
            int numtimesteps = resultData.NumberOfTimeSteps;
            DateTime startTime = resultData.StartTime;
            DateTime endTime = resultData.EndTime;
            // times for all timesteps
            IListDateTimes times = resultData.TimesList;

            // Results are stored in DataItem’s, where one DataItem contains e.g. water level.
            // Nodes, Reaches, Catchments and GlobalData of the resultData object will have a 
            // number of data items.
            // A data item will store data for a number of elements and for a number
            // of time steps. 
            // In reaches there will be more than one "element", since a reach will store 
            // data for several grid points. Nodes and catchments will always only have 
            // one element per data item

            IRes1DNodes nodes = resultData.Nodes;
            IRes1DReaches reaches = resultData.Reaches;
            IRes1DCatchments catchments = resultData.Catchments;
            IRes1DGlobalData globals = resultData.GlobalData;



            if (args.Length == 3)
            {//if only input is a result file then write an M11out file. 
                throw new Exception("not enough parameters.");
                StreamWriter outFile = new("M11.out");

                foreach (IRes1DNode ir in nodes)
                {
                    Type xx = ir.GetType();
                    if (xx.Name == "Res1DManhole")
                    {
                        outFile.WriteLine("0   103 Node_WL:  <" + ir.Id + ">");
                    }

                }

                outFile.Close();
                return;
            }

            
           
            if (args.Length > 3)
            {
                int[] nodeNs = null; // contains the index of all nodes that is to be treated
                int[] reachNs = {};

                if(args.Length == 4)
                {// all links and nodes are to be treated as no selection file is present.
                    nodeNs = new int[nodes.Count];
                    for (int i = 0; i < nodes.Count; i++) nodeNs[i] = i;
                    
                    reachNs = new int[reaches.Count];
                    for (int i = 0; i < reaches.Count; i++) reachNs[i] = i;

                }
                else
                {   //****************************************************************
                    //read mus and calculate which nodes and reaches should be treated
                    Console.WriteLine("Comparing");
                    MusFileContentx musSelect = new(args[4]);
                    List<int> tempNodeNs = new List<int>();
                    List<int> tempReachNs = new List<int>();

                    //TODO: remake the selection to be based on sorted list of all res file elements, which will speed this part up immensly.
                    for (int i = 0; i < nodes.Count; i++)  if(MusFileContentx.ArrayContainsString(musSelect.Nodes, nodes[i].Id)) tempNodeNs.Add(i);
                    nodeNs = tempNodeNs.ToArray();

                    for (int i = 0; i < reaches.Count; i++)
                    {   
                        
                        if( reaches[i] is IRes1DStructureReach)
                        {// is pump, weir og oriface
                            IRes1DStructureReach irs = (IRes1DStructureReach)reaches[i];
                            
                            if (string.Compare(irs.Id, 0, @"Weir:", 0, 4) == 0)
                            {
                                if (MusFileContentx.ArrayContainsString(musSelect.Weirs, irs.Name.Substring(5))) tempReachNs.Add(i);
                            }
                            else if (string.Compare(irs.Id, 0, @"Pump:", 0, 4) == 0)
                            {
                                if (MusFileContentx.ArrayContainsString(musSelect.Pumps, irs.Name.Substring(5))) tempReachNs.Add(i);

                            }
                            else if (string.Compare(irs.Id, 0, @"Orifice:", 0, 7) == 0)
                            {
                                if (MusFileContentx.ArrayContainsString(musSelect.Orifices, irs.Name.Substring(8))) tempReachNs.Add(i);

                            }
                            else if (string.Compare(irs.Id, 0, @"Valve:", 0, 5) == 0)
                            {
                                if (MusFileContentx.ArrayContainsString(musSelect.Valves, irs.Name.Substring(6))) tempReachNs.Add(i);

                            }
                            else throw new Exception("Could not regonize structure type of " + irs.Name);

                        }
                        else
                        {// is normal link
                            if (MusFileContentx.ArrayContainsString(musSelect.Links, reaches[i].Name)) tempReachNs.Add(i);

                        }
                    }

                    reachNs = tempReachNs.ToArray();

                }
                //*********************************************************

                float[,] nodesStats = new float[4, nodes.Count];//0: accumulated, 1: average, 2: max (2: min, 3: max)
                double[,] linkStats = new double[6, reaches.Count];//0: accumulated, 1: average, 2: max, (2: min, 3: max, 4: accum negative, 5 accum positive)
                int numberOfTimeSteps = nodes[0].DataItems[0].NumberOfTimeSteps;

                Console.WriteLine("Writing");
                StreamWriter resFile = new(args[3]);
                //writing header
                resFile.Write("Date" + "\t" + "time" + "\t");
                //for (int n = 0; n < nodes.Count; n++) resFile.Write(nodes[n].Id + "\t");
                foreach(int n in nodeNs) resFile.Write(nodes[n].Id + "\t");
                //for (int n = 0; n < reaches.Count; n++) resFile.Write(reaches[n].Id + "\t");
                foreach (int n in reachNs) resFile.Write(reaches[n].Id + "\t");

                resFile.WriteLine("");
                resFile.Write("," + "\t" + "," + "\t");
                //for (int n = 0; n < nodes.Count; n++) resFile.Write("Node_WL" + "\t");
                foreach (int n in nodeNs) resFile.Write("Node_WL" + "\t");
                //for (int n = 0; n < reaches.Count; n++) resFile.Write("Flow" + "\t");
                foreach (int n in reachNs) resFile.Write("Flow" + "\t");
                resFile.WriteLine("");

                bool doWrite = false;

                int iStep = 0;
                float[] xxNodes = new float[nodes.Count];//temp var for calculating aggrerated values
                float[] xxLinks = new float[reaches.Count];

                TimeSpan timeStepDur = times[20] - times[0];
                double secondsPrTimeStep_default = timeStepDur.TotalSeconds/20; //Assuming no large jumps in time in the beginning. If the time stamps jump more than 4 times the default, the default is used. Such large jumps should only occur in LTS results. 
                double secondPrTimeStep = 0;
                TimeSpan tspTotal = times[times.Count - 1] - times[0];
                double totalTimeSpanInSeconds = tspTotal.TotalSeconds;

                //writing data
                for (int j = 0; j < numberOfTimeSteps ; j++)
                {   
                    iStep++;
                    if (iStep == NstepAggregate)
                    { 
                        doWrite = true;
                        iStep = 0;
                    }
                    else
                    {
                        doWrite = false;
                    }

                    if(j==0 || j>times.Count - 2)
                    {
                        secondPrTimeStep = secondsPrTimeStep_default;
                    }
                    else
                    {   // assuming the value at time j represent the flow in the invertal midway between both neighbouring points. 
                        timeStepDur = times[j+1] - times[j-1];  
                        secondPrTimeStep = timeStepDur.TotalSeconds/2;
                    }


                    if(doWrite) resFile.Write(times[j].ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture) + "\t" + times[j].ToString("HH:mm", System.Globalization.CultureInfo.InvariantCulture) + "\t");

                    //Nodes
                    //for (int n = 0; n < nodes.Count; n++) 
                    foreach (int n in nodeNs)
                    {
                        float xx = nodes[n].DataItems[0].TimeData.GetValue(j, 0);
                        nodesStats[0, n] += xx;

                        if (j == 0)
                        {
                           nodesStats[3, n] = xx;
                           nodesStats[2, n] = xx;
                        }


                        if (nodesStats[3, n] < xx) nodesStats[3, n] = xx;
                        if (nodesStats[2, n] > xx) nodesStats[2, n] = xx;

                        if (NstepAggregate > 1)
                        {
                            if (bAggregateAsMax)
                            {
                                if(xx > xxNodes[n]) xxNodes[n] = xx;
                            }
                            else
                            {
                                   xxNodes[n] += xx;
                            }
                            
                            
                            if(doWrite)
                            {
                                if (bAggregateAsMax)
                                {
                                    xx = xxNodes[n];
                                }
                                else
                                {
                                       xx = xxNodes[n] / NstepAggregate;
                                }
                                xxNodes[n] = 0;                                
                            }
                        }


                        if (doWrite) resFile.Write(xx + "\t");

                    }
                    //Reaches (only first Q point)
                    //for (int n = 0; n < reaches.Count; n++)
                    double xx_acc = 0;//time accumulated 
                        foreach (int n in reachNs)
                        {
                        float xx = reaches[n].DataItems[1].TimeData.GetValue(j,0);
                        xx_acc = xx * secondPrTimeStep;

                        linkStats[0, n] += xx_acc;
                        if (xx < 0)
                        {
                            linkStats[4, n] += xx_acc;
                        }
                        else linkStats[5, n] += xx_acc;

                        if (j == 0)
                        {
                            linkStats[3, n] = xx;
                            linkStats[2, n] = xx;

                        }


                        if (linkStats[3, n] < xx) linkStats[3, n] = xx;
                        if (linkStats[2, n] > xx) linkStats[2, n] = xx;

                        if (NstepAggregate > 1)
                        {

                            if (bAggregateAsMax)
                            {
                                if (xx > xxLinks[n]) xxLinks[n] = xx;
                            }
                            else
                            {
                                xxLinks[n] += xx;
                            }
                            

                            if (doWrite)
                            {

                                if (bAggregateAsMax)
                                {
                                    xx = xxLinks[n];
                                }
                                else
                                {
                                    xx = xxLinks[n] / NstepAggregate;
                                }
                                
                                xxLinks[n] = 0;
                            }
                        }


                        if (doWrite) resFile.Write(xx + "\t");

                    }
                    if (doWrite) resFile.WriteLine("");
                }

                resFile.Close();

                // for (int n = 0; n < nodes.Count; n++)
                foreach (int n in nodeNs)
                {
                nodesStats[1, n] = nodesStats[0, n] / numberOfTimeSteps;
            }

            StreamWriter statsFile = new("nodesStats.txt");
            statsFile.WriteLine("MUID" + "\t" + "average" + "\t" + "min" + "\t" + "max");
                // for (int n = 0; n < nodes.Count; n++)
                foreach (int n in nodeNs)
                {
                statsFile.WriteLine(nodes[n].Id + "\t" + nodesStats[1, n] + "\t" + nodesStats[2, n]+  "\t" + nodesStats[3, n]);
            }
            statsFile.Close();

                //for (int n = 0; n < reaches.Count; n++)

                    foreach (int n in reachNs)
                    {
                    //linkStats[1, n] = linkStats[0, n] / numberOfTimeSteps;
                    linkStats[1, n] = linkStats[0, n] / totalTimeSpanInSeconds;
                }
                statsFile = new("linksStats.txt");

                
                statsFile.WriteLine("MUID" + "\t" + "\t" + "Accumulated[m3]" + "\t" + "average[m3/s]" + "\t" + "min[m3/s]" + "\t" + "max[m3/s]" + "\t"  + "AccNegative[m3]" + "\t" + "AccPositive[m3]");
                //for (int n = 0; n < reaches.Count; n++)
                foreach (int n in reachNs)
                {
                    statsFile.WriteLine(reaches[n].Id + "\t" + (float)linkStats[0, n]  + "\t" + (float)linkStats[1, n] + "\t" + (float)linkStats[2, n] + "\t" + (float)linkStats[3, n] + "\t" + (float)linkStats[4, n]  + "\t" + (float)linkStats[5, n] );
                }
                statsFile.Close();

             }                  
            return;
        }
    }
}
