﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrugerRes1dExtra
{
    public class MusFileContentx
    {
        public string[] Links;
        public string[] Nodes;
        public string[] Weirs;
        public string[] Pumps;
        public string[] Orifices;
        public string[] Valves;


        public MusFileContentx(string musFileNameFullPath)
        {
            //msm_Link
            //msm_Node


            // booleans showing the current section of the mus file. 
            bool bInNodes = false;
            bool bInLinks = false;
            bool bInWeirs = false;
            bool bInOrifes = false;
            bool bInPumps = false;
            bool bInValves = false;

            List<string> tempLinks = new List<string>();
            List<string> tempNodes = new List<string>();
            List<string> tempWeirs = new List<string>();
            List<string> tempOrifes = new List<string>();
            List<string> tempPumps = new List<string>();
            List<string> tempValves = new List<string>();


            foreach (string line in System.IO.File.ReadLines(musFileNameFullPath))
            {


                switch (line)
                {

                    case "\r":
                    case "":
                        bInNodes = false;
                        bInLinks = false;
                        bInWeirs = false;
                        bInOrifes = false;
                        bInPumps = false;
                        bInValves = false;
                        break;

                    case "msm_Link":
                        bInNodes = false;
                        bInLinks = true;
                        bInWeirs = false;
                        bInOrifes = false;
                        bInPumps = false;
                        bInValves = false;
                        break;

                    case "msm_Node":
                        bInNodes = true;
                        bInLinks = false;
                        bInWeirs = false;
                        bInOrifes = false;
                        bInPumps = false;
                        bInValves = false;
                        break;

                    case "msm_Weir":
                        bInNodes = false;
                        bInLinks = false;
                        bInWeirs = true;
                        bInOrifes = false;
                        bInPumps = false;
                        bInValves = false;
                        break;

                    case "msm_Pump":
                        bInNodes = false;
                        bInLinks = false;
                        bInWeirs = false;
                        bInOrifes = false;
                        bInPumps = true;
                        bInValves = false;
                        break;

                    case "msm_Orifice":
                        bInNodes = false;
                        bInLinks = false;
                        bInWeirs = false;
                        bInOrifes = true;
                        bInPumps = false;
                        bInValves = false;
                        break;

                    case "msm_Valve":
                        bInNodes = false;
                        bInLinks = false;
                        bInWeirs = false;
                        bInOrifes = false;
                        bInPumps = false;
                        bInValves = true;
                        break;



                    default:
                        //Console.WriteLine("Nothing");
                        if (bInNodes) 
                        {
                            tempNodes.Add(line);
                        }
                        else if (bInLinks)
                        {
                            tempLinks.Add(line);
                        }
                        else if (bInPumps)
                        {
                            tempPumps.Add(line);
                        }
                        else if (bInWeirs)
                        {
                            tempWeirs.Add(line);
                        }
                        else if (bInOrifes)
                        {
                            tempOrifes.Add(line);
                        }
                        else if (bInValves)
                        {
                            tempValves.Add(line);
                        }

                        break;

                }


            }

            Nodes = tempNodes.ToArray();
            Links = tempLinks.ToArray();
            Pumps = tempPumps.ToArray();
            Weirs = tempWeirs.ToArray();
            Orifices = tempOrifes.ToArray();
            Valves = tempValves.ToArray();

        }

        public static bool ArrayContainsString(string[] arrayOfStrings, string testString)
        {
            for (int j = 0; j <arrayOfStrings.Length; j++)
            {
                if (testString == arrayOfStrings[j])
                {
                    return true;
                }
            }
            return false;
        }

    }
}
