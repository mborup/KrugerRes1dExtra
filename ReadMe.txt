The KrugerRes1DExtra program is a stand-alone .exe that extracts results from res1d files and saves as txt files. 

It requires .net5

 


Syntax:
KrugerRes1DExtra.exe nStepAggregation=30 asMax=true "full path to res1d file" "output file name (only name)" "full path to .mus file (optional)"

Parameters:
nStepAggregation=30 means that only a single value is saved for each location for each 30 time steps. If asMax=true then the maximum value of these 30 time steps is saved. If asMax=false then the average of the 30 time steps is saved.